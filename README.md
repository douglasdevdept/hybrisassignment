# README #

This document will explain the task expected for hiring process at Douglas for Hybris or SAP Commerce developers

### Task ###

a) Provide a solution/code snippet to ensure that a Media file which is attached for a particular user can only be accessed by this user

b) Design and provide code snippet for a business process which does the below tasks

	1. Sends a message to a third party system(you can mock this)
	2. Receive the message in an asynchronous fashion
	
c) Provide a solution/code snippet to override the existing authentication oauth system of the webservices with a mock system or open source solution

d) Provide a solution/code snippet add a custom extension and do the below process
	1.	Add a new attribute to product
	2.	Write a custom service to get all products with this attribute
	
	


### Things to consider ###

* Follow standard process
* Assume the solution is provided for a production system
* Follow guidelines and best practices

### Next Steps ###

* You can share the solution in an appropriate way to the team
* Based on the solution and a feedback Douglas will contact you